import { cleanup, render } from "@testing-library/react";
import React from "react";
import { Button } from "./Button";

const testId = "test-btn";

describe("Button", () => {
  afterEach(cleanup);

  it("has fontSize 'medium' by default", () => {
    const { getByTestId } = render(
      <Button data-testid={testId}>Test Button</Button>
    );

    const btn = getByTestId(testId);
    expect(btn).toHaveStyle("font-size: medium");
  });

  it("can adjust fontSize to 'large'", () => {
    const { getByTestId } = render(
      <Button fontSize="large" data-testid={testId}>
        Test Button
      </Button>
    );

    const btn = getByTestId(testId);
    expect(btn).toHaveStyle("font-size: large");
  });
});
