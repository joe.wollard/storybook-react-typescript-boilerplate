import React, { ButtonHTMLAttributes } from "react";

export interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  /**
   * Adjust the font size of the button.
   * @default "medium"
   */
  fontSize?: "large" | "medium";
}

export function Button({ fontSize = "medium", ...props }: ButtonProps) {
  return <button type="button" {...props} style={{ fontSize }} />;
}
