import { withA11y } from "@storybook/addon-a11y";
import { withTests } from "@storybook/addon-jest";
import { INITIAL_VIEWPORTS } from "@storybook/addon-viewport";
import { addDecorator, addParameters } from "@storybook/react";
import results from "../.jest-test-results.json";
import "../scss/app.scss";

addParameters({
  a11y: {
    element: "#root",
  },
  viewport: {
    viewports: INITIAL_VIEWPORTS,
  },
});

addDecorator(withTests({ results }));
addDecorator(withA11y);
