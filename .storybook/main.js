const path = require("path");

module.exports = {
  stories: ["../stories/**/*.stories.(tsx|mdx)"],
  addons: [
    "@storybook/addon-docs",
    "@storybook/addon-a11y/register",
    "@storybook/addon-jest/register",
    "@storybook/addon-viewport/register"
  ],
  webpackFinal: async config => {
    config.module.rules.push({
      test: /\.(ts|tsx)$/,
      include: [
        path.resolve(__dirname, "../src"),
        path.resolve(__dirname, "../stories")
      ],
      use: [
        require.resolve("babel-loader"),
        {
          loader: require.resolve("react-docgen-typescript-loader"),
          options: {
            tsconfigPath: path.resolve(__dirname, "../tsconfig.json"),
            propFilter: function(prop) {
              if (prop.parent) {
                return !prop.parent.fileName.includes("node_modules");
              }
              return true;
            }
          }
        }
      ]
    });

    // Add scss support
    config.module.rules.push({
      test: /\.scss$/,
      use: ["style-loader", "css-loader", "sass-loader"],
      include: path.resolve(path.join(__dirname, "..", "scss"))
    });

    config.resolve.extensions.push(".ts", ".tsx", ".scss");
    return config;
  }
};
